package antion.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.beans.Transient;

public class Ellipse extends Figure{

    public Ellipse(double x, double y, int intRandom, int random){
        super();
        init();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    private void setVisible(boolean b) {
    }

    private void setDefaultCloseOperation(int exitOnClose) {
    }

    private void pack() {
    }

    public Ellipse(int type, double cx, double cy, double lineWidth) {
        super(type, cx, cy, lineWidth);
    }

    private void init() {
        final Point p1 = new Point(210, 280);
        final Point p2 = new Point(160, 190);
        final Ellipse2D.Double el = new Ellipse2D.Double(p1.x > p2.x ? p2.x : p1.x,
                p1.y > p2.y ? p2.y : p1.y,
                Math.abs(p1.x - p2.x),
                Math.abs(p1.y - p2.y));

        JPanel p = new JPanel(){
            protected void paintComponent(java.awt.Graphics g) {
                super.paintComponent(g);
                Graphics2D g2d = (Graphics2D) g;
                g2d.draw(el);
                g2d.setColor(Color.RED);
                g2d.drawString("P1", p1.x, p1.y);
                g2d.drawString("P2", p2.x, p2.y);
            };

            @Override
            @Transient
            public Dimension getPreferredSize() {
                return new Dimension(p1.x+p2.x+10,p1.y+p2.y+10);
            }
        };

        add(p);
    }

    private void add(JPanel p) {
    }

    public static void main(String[] args) {
        new Ellipse();
    }

    @Override
    public void draw(GraphicsContext gc) {
        
    }
}