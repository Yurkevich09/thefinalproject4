package antion.figuresfx.figures;

import antion.figuresfx.drawUtils.Drawable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public abstract class Figure implements Drawable {
    public static final int FIGURE_TYPE_CIRCLE = 0;
    public static final int FIGURE_TYPE_RECTANGLE = 1;
    public static final int FIGURE_TYPE_TRIANGLE = 2;
    public static final int FIGURE_TYPE_ELLIPSE = 3;
    public static final int FIGURE_TYPE_STAR = 4;




    private int type;
    protected double cx;
    protected double cy;
    protected double lineWidth;
    protected Color color;

    public Figure(int type, double cx, double cy, double lineWidth, Color color) {
        this.type = type;
        this.cx = cx;
        this.cy = cy;
        this.lineWidth = lineWidth;
        this.color = color;
    }

    public int getType() {
        return type;
    }

    public double getCx() {
        return cx;
    }

    public void setCx(double cx) {
        this.cx = cx;
    }

    public double getCy() {
        return cy;
    }

    public void setCy(double cy) {
        this.cy = cy;
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(GraphicsContext gc);
}

   





package antion.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Star extends Figure {
    private double radius;

    public Star(double cx, double cy, double lineWidth, Color color) {
        super(FIGURE_TYPE_STAR, cx, cy, lineWidth, color);
    }

    public Star(double cx, double cy, double lineWidth, Color color, double radius) {
        this(cx, cy, lineWidth, color);
        this.radius = radius < 10 ? 10 : radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Star star = (Star) o;
        return Double.compare(star.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(radius);
    }

    @Override
    public String toString() {
        return "antion.figuresfx.figures.Star{" +
                "radius=" + radius +
                '}';
    }

    public double sin(double angle) {
        angle = Math.sin(Math.toRadians(angle));
        return angle;
    }

    public double cos(double angle) {
        angle = Math.cos(Math.toRadians(angle));
        return angle;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokePolygon(
                new double[]{
                        cx,
                        cx - sin(36) * radius / 2,
                        cx - cos(18) * radius,
                        cx - cos(18) * radius / 2,
                        cx - sin(36) * radius,
                        cx,
                        cx + sin(36) * radius,
                        cx + cos(18) * radius / 2,
                        cx + cos(18) * radius,
                        cx + sin(36) * radius / 2
                }, new double[]{
                        cy - radius,
                        cy - cos(36) * radius / 2,
                        cy - sin(18) * radius,
                        cy + sin(18) * radius / 2,
                        cy + cos(36) * radius,
                        cy + radius / 2,
                        cy + cos(36) * radius,
                        cy + sin(18) * radius / 2,
                        cy - sin(18) * radius,
                        cy - cos(36) * radius / 2
                }, 10
        );
    }
}


