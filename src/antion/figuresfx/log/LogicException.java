package antion.figuresfx.log;

import java.util.logging.Logger;

public class LogicException {
    private static final Logger log = (Logger) Logger.getLogger(String.valueOf(LogicException.class));
    private String classExceptionName;

    public void doException(String classExceptionName){
        this.classExceptionName = classExceptionName;
        log.info(classExceptionName + "it was thrown");
        addLogException();
    }

    public void addLogException(){
        if (classExceptionName.equals("UnknownTypeFigureException")){
            log.info("Type of figure doesn*t match with ...");
        }
        else if (classExceptionName.equals("EmptyArrayException")){
            log.info("Array is empty...");
        }
        else {
            log.info("Java exception...");
        }
    }
}
