package com.antion.figuresfx.exception;

public class EmptyArrayException extends Exception {
    public EmptyArrayException() {
        printStackTrace();
    }

    @Override
    public String toString() {
        return "antion.figuresfx.exception.EmptyArray";
    }
}

